package common

import (
	"bufio"
	"io"
	"strings"
)

// ReadLine read a line from reader, trim new line
func ReadLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}
	return strings.TrimRight(string(str), "\r\n")
}
