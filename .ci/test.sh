#!/bin/bash

# can get coverage via regex total:\s+\(statements\)\s+(\d+.\d+\%)
go test -race $(go list ./... | grep -v '/vendor/') \
  -v \
  -coverprofile=coverage.out \
  -tags test

go tool cover -func=coverage.out
