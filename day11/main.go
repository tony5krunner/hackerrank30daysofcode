package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	var arr [][]int32
	for i := 0; i < 6; i++ {
		arrRowTemp := strings.Split(readLine(reader), " ")

		var arrRow []int32
		for _, arrRowItem := range arrRowTemp {
			arrItemTemp, err := strconv.ParseInt(arrRowItem, 10, 64)
			checkError(err)
			arrItem := int32(arrItemTemp)
			arrRow = append(arrRow, arrItem)
		}

		if len(arrRow) != int(6) {
			panic("Bad input")
		}

		arr = append(arr, arrRow)
	}

	fmt.Println(largestHourglassSum(arr))
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func largestHourglassSum(arr [][]int32) int32 {
	var maxSum int32 = math.MinInt32
	hSize := int32(3)
	aSize := int32(len(arr[0]))
	z0 := int32(0)
	for i := z0; i < aSize-hSize+1; i++ {
		for j := z0; j < aSize-hSize+1; j++ {
			sum := z0
			for i1 := z0; i1 < hSize; i1++ {
				for j1 := z0; j1 < hSize; j1++ {
					if i1 != 1 || (j1 == 1 && i1 == 1) {
						sum += arr[i1+i][j1+j]
					}
				}
			}
			if sum > maxSum {
				maxSum = sum
			}
		}
	}

	return maxSum
}
