//+build !test

package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/phuonghuynh-eng/hackerrank30daysofcode/common"
)

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1<<20)
	line := common.ReadLine(reader)

	fmt.Println(resolve(line))
}
